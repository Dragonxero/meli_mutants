# Mutant API

## Algoritmos
 - Las funciones encargadas de la evaluacion del gen mutante se encuentran en un modulo en la carpeta lib, llamado detector.

## Ejecucion
 - El API esta actualmente en un dyno de Heroku, la cual posee la siguiente direccion https://mutant-api-apply-1412.herokuapp.com/, dado que es un dyno free se recomienda consultar la pagina via browser para despertar en dyno y que las consultas a los endpoint no tengan tanta demora.
 - Los endpoint disponibles son:
    - /mutant 
      Descripcion: recibe un POST con una cadena de ADN a evaluar para deteccion de gen mutante.
      URL: https://mutant-api-apply-1412.herokuapp.com/mutant/
      Method: POST
      Body: { "adn": ["AAAGGG","CCCTTT","AAAGGG","CCCTTT","AAAGGG","CCCTTT"] }

    - /stats 
      Description: El cual permite consultar las estadisticas de las cadenas de adn verificadas tanto mutantes como de humano normal.
      URL: https://mutant-api-apply-1412.herokuapp.com/stats
      Method: GET
      Body: N/A

