class PeopleController < ApplicationController
  before_action :set_person, only: [:show, :update, :destroy]
  
  include Detector
  # POST /mutant
  def create
    @person = Person.new
    adn = params[:adn]
    @person.adn = adn.join("")
    count = Person.where(adn: @person.adn).count
    if count==0
      if isMutant(adn)
        puts "MUTANT"
        @person.is_mutant = true
        @person.save
        render status: :ok
      else
        puts "NOT MUTANT!!!!!!!!!!!"
        @person.is_mutant = false
        @person.save
        render status: :forbidden
      end
    else
      render status: :forbidden
    end
  end

  def stats
    people = Person.count
    mutants = Person.where(is_mutant: true).count
    ratio = people>0 ? (mutants.to_f/people.to_f).round(2) : 0
    response = {:count_mutant_dna => mutants, :count_human_dna => people, :ratio => ratio }
    render json: response, status: :ok
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_person
      @person = Person.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def person_params
      params.require(:person).permit(:adn)
    end
end