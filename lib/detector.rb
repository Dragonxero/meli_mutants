module Detector
  def isMutant(adn)
    cont = 0
    #Primero comprobar que es una cadena valida y matriz cuadrada
    if(isValidBases(adn))
      if(isMatrixSquare(adn))
        cont = findMutantGEN(adn) + findMutantGEN(transformRowsintoColumns(adn)) + findMutantGEN(extractDiagonals(adn))
        if cont>2
          return true
        else
          return false
        end
      else
        return false
      end
    else
      return false
    end
  end

  def findMutantGEN(adn)
    cont = 0
    mutant_chains = %w[AAAA CCCC TTTT GGGG]
    adn.each do |string|
      mutant_chains.each do |gen|
        if string.include? gen # AAAA CCCC TTTT GGGG]
          cont += 1
        end
      end
    end
    return cont
  end

  def transformRowsintoColumns(adn)
    transformed = Array.new(adn.length){Array.new(adn.length)}
    for i in 0..adn.length-1
      for j in 0..adn.length-1
        transformed[j][i] = adn[i][j]
      end
      transformed[i] = transformed[i].join("")
    end
    return transformed
  end

  def extractDiagonals(adn)
    diagonals = Array.new(2){Array.new(adn.length)}
    for i in 0..adn.length-1
      # from 0,0 to n,n
      diagonals[0][i] = adn[i][i]
      #from 0,n to n,0
      diagonals[1][i] = adn[i][adn.length-1-i]
    end
    for i in 0..1
      diagonals[i] = diagonals[i].join("")
    end
    return diagonals
  end

  def isMatrixSquare(adn)
    return adn.length == adn[0].size
  end

  def isValidBases(adn)
    serial = adn.join("")
    return serial.count("ACGT") == serial.length
  end
end