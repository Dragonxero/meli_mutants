Rails.application.routes.draw do
  post '/mutant', to: 'people#create', as: '/'
  get '/stats', to:'people#stats', as: '/'
end
