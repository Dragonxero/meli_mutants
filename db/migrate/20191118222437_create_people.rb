class CreatePeople < ActiveRecord::Migration[5.2]
  def change
    create_table :people do |t|
      t.string :adn, unique: true
      t.boolean :is_mutant
    end
  end
end
